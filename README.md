Earley Parser in Go
===================

This is an Earley parser in Go.  It is a port of the version from
RScheme (which can be found in the
[html](https://github.com/dkolbly/rscheme/tree/master/lib/rs/net/html/parse)
directory, although I'm sure that was just a copy from wherever I was
working on my dissertation, now mostly lost to the mists of time)

Specifically, the purpose of this parser is *not* to be the fastest
recognizer, or even the fastest parser.  It's purpose is to be easily
extensible, in particular to create an extensible language -- in my
context, that means to be able to change the grammar on the fly, as a
result of reducing productions within the grammar.  From this we can
get macros :-D

But for now, it's mostly focused on being easy to use and easy to plug
in to libraries for use in writing DSLs.
