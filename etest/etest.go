package main

import (
	"context"
	"math/big"
	"os"

	"bitbucket.org/dkolbly/earley"
	"github.com/dkolbly/logging"
	"github.com/dkolbly/logging/pretty"
)

var log = logging.New("etest")

func main() {
	if true {
		pretty.Debug()
	}

	if false {
		fd, err := os.Open("test.e")
		if err != nil {
			log.Fatal(err)
		}
		ss := earley.NewSimple("test.e", fd)
		for {
			tok, err := ss.Next()
			if err != nil {
				log.Error("All done: %s", err)
				break
			}
			log.Info("Token: %s", tok)
		}
	}
	for _, src := range os.Args[1:] {
		lang := makeGrammar()
		fd, err := os.Open(src)
		if err != nil {
			log.Fatal(err)
		}
		m, err := lang.Parse(context.Background(), fd, nil)
		if err != nil {
			log.Fatal(err)
		}
		log.Info("Result: %#v", m)
		if i, ok := m[0].(*earley.Integer); ok {
			log.Info("Answer is %s", i.Value)
		}
	}
}

type IntMatcher struct {
}

/*func (im IntMatcher) terminal() bool {
	return false
}*/

func (im IntMatcher) String() string {
	return "INTEGER"
}

func (im IntMatcher) Match(t earley.Token) bool {
	if _, ok := t.(*earley.Integer); ok {
		return true
	}
	return false
}

func makeGrammar() *earley.Lang {
	/*intvalue := func(tok *earley.Integer) *big.Int {
		return tok.Value
	}*/

	intvalue := func(e earley.Envt, m []earley.Meaning) (earley.Envt, earley.Meaning) {
		return e, m[0].(*earley.Integer).Value
	}

	add2 := func(e earley.Envt, m []earley.Meaning) (earley.Envt, earley.Meaning) {
		log.Info("add2(%#v)", m)
		c := new(big.Int)
		c.Add(m[0].(*big.Int), m[2].(*big.Int))
		return e, c
		//return e, m[0].(int) + m[2].(int)
	}

	mul := func(e earley.Envt, m []earley.Meaning) (earley.Envt, earley.Meaning) {
		c := new(big.Int)
		a := m[0].(*big.Int)
		b := m[2].(*big.Int)
		c.Mul(a, b)
		return e, c
	}

	identity := func(e earley.Envt, m []earley.Meaning) (earley.Envt, earley.Meaning) {
		return e, m[0]
	}

	expr := earley.Intern("expr")
	term := earley.Intern("term")
	factor := earley.Intern("factor")

	lang := earley.NewLang(expr, earley.SimpleFactory{})

	lang.Add(lang.Rule(term, identity, factor))
	lang.Add(lang.Rule(term, add2, factor, "+", term))

	lang.Add(lang.Rule(expr, identity, term))

	lang.Add(lang.Rule(factor, mul, factor, "*", factor))
	lang.Add(lang.Rule(factor, intvalue, IntMatcher{}))
	return lang
}
