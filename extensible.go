package earley

type ExtendedGrammar struct {
	parent Envt
	here   map[Symbol][]*Production
}

func Extend(from Envt, rules ...*Production) *ExtendedGrammar {
	h := make(map[Symbol][]*Production)

	for _, rule := range rules {
		nt := rule.NonTerminal
		h[nt] = append(h[nt], rule)
	}

	return &ExtendedGrammar{
		parent: from,
		here:   h,
	}
}
