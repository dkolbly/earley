package earley

import (
	"fmt"
	"math/big"
)

type Integer struct {
	At    Posn
	Value *big.Int
}

func (i *Integer) ItemString() string {
	return i.Value.String()
}

func (i *Integer) Int64() int64 {
	return i.Value.Int64()
}

func (i *Integer) Equal(t Token) bool {
	if i2, ok := t.(*Integer); ok {
		return i.Value.Cmp(i2.Value) == 0
	}
	return false
}

func (i *Integer) String() string {
	return fmt.Sprintf("@%d:%d INT{%s}", i.At.Line, i.At.Column,
		i.Value.String())
}
