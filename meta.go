package earley

import (
	"reflect"
)

func (lang *Lang) nameof(arg interface{}) string {
	switch arg := arg.(type) {
	case string:
		return arg
	case Symbol:
		return arg.String()
	default:
		panic("unsupported arg for optional()")
	}
}

// Optional builds a rule that produces either nothing
// or the given rule
func (lang *Lang) Optional(arg interface{}) Symbol {
	opt := Intern(lang.nameof(arg) + "?")
	lang.Add(lang.Rule(opt, identity, arg))
	lang.Add(lang.Rule(opt, nothing))
	return opt
}

type Optional struct {
	Set   bool
	Value Meaning
}

func nothing(e Envt, m []Meaning) (Envt, Meaning) {
	return e, Optional{}
}

func identity(e Envt, m []Meaning) (Envt, Meaning) {
	return e, Optional{true, m[0]}
}

// Star builds a rule that produces 0 or more things, whose meaning is
// a slice of the given prototype type.  The arg should be either a
// symbol (non-terminal which produces the right meanings) or a string
// for a literal (why would you have a star of a literal? :/)
func (lang *Lang) Star(slice interface{}, arg interface{}) Symbol {
	t := reflect.TypeOf(slice)
	if t.Kind() != reflect.Slice {
		panic("Star() not given a slice type")
	}

	nt := Intern(lang.nameof(arg) + "*")
	concat := func(e Envt, m []Meaning) (Envt, Meaning) {
		prefix := reflect.ValueOf(m[0])
		appending := reflect.ValueOf(m[1])
		extended := reflect.Append(prefix, appending)
		return e, extended.Interface()
	}
	empty := func(e Envt, m []Meaning) (Envt, Meaning) {
		// the empty case... create an empty slice
		return e, reflect.MakeSlice(t, 0, 0).Interface()
	}

	lang.Add(lang.Rule(nt, concat, nt, arg))
	lang.Add(lang.Rule(nt, empty))
	return nt
}

// Separated builds a rule that produces 0 or more things, which is either empty
// or a delimiter-separated sequence of things
func (lang *Lang) Separated(slice interface{}, arg interface{}, sep interface{}) Symbol {
	return lang.sep(slice, arg, sep, true)
}

func (lang *Lang) SeparatedNonEmpty(slice interface{}, arg interface{}, sep interface{}) Symbol {
	return lang.sep(slice, arg, sep, false)
}

func (lang *Lang) sep(slice interface{}, arg interface{}, sep interface{}, emptyOk bool) Symbol {
	st := reflect.TypeOf(slice)
	if st.Kind() != reflect.Slice {
		panic("Seperated() not given a slice type")
	}

	nonempty := Intern(lang.nameof(arg) + "+")

	concat := func(e Envt, m []Meaning) (Envt, Meaning) {
		prefix := reflect.ValueOf(m[0])
		appending := reflect.ValueOf(m[2])

		extended := reflect.Append(prefix, appending)
		return e, extended.Interface()
	}
	empty := func(e Envt, m []Meaning) (Envt, Meaning) {
		// the empty case... create an empty slice
		return e, reflect.MakeSlice(st, 0, 0).Interface()
	}
	single := func(e Envt, m []Meaning) (Envt, Meaning) {
		// the single case... create an slice with one thing
		s := reflect.MakeSlice(st, 0, 1)
		one := reflect.ValueOf(m[0])
		return e, reflect.Append(s, one).Interface()
	}
	identity := func(e Envt, m []Meaning) (Envt, Meaning) {
		return e, m[0]
	}

	lang.Add(lang.Rule(nonempty, concat, nonempty, sep, arg))
	lang.Add(lang.Rule(nonempty, single, arg))
	if emptyOk {
		nt := Intern(lang.nameof(arg) + "#")
		lang.Add(lang.Rule(nt, identity, nonempty))
		lang.Add(lang.Rule(nt, empty))
		return nt
	} else {
		return nonempty
	}
}
