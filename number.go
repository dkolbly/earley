package earley

import (
	"fmt"

	"github.com/shopspring/decimal"
)

type Decimal struct {
	At    Posn
	Value decimal.Decimal
}

func (i *Decimal) ItemString() string {
	return i.Value.String()
}

func (i *Decimal) Equal(t Token) bool {
	if i2, ok := t.(*Decimal); ok {
		return i.Value.Cmp(i2.Value) == 0
	}
	return false
}

func (i *Decimal) String() string {
	return fmt.Sprintf("@%d:%d DEC{%s}", i.At.Line, i.At.Column,
		i.Value.String())
}

func (l *Decimal) FromLexeme(at Posn, lex [][]byte) Token {

	n, err := decimal.NewFromString(string(lex[0]))
	if err != nil {
		panic(err)
	}
	return &Decimal{
		At:    at,
		Value: n,
	}
}
