package earley

import (
	"bytes"
	"context"
	"errors"
	"fmt"
	"io"
	"reflect"
)

var Trace = false

// this is for debugging... TODO get rid of it or make it real
var shortptr = map[uintptr]int{}

func shorten(p uintptr) int {
	if s, ok := shortptr[p]; ok {
		return s
	}
	s := len(shortptr) + 10
	shortptr[p] = s
	return s
}

// An Envt represents an environment in which expressions
// (and the non-terminals) are evaluated
type Envt interface {
	Productions(Symbol) []*Production
	Context() context.Context
}

// A Meaning represents the bottom-up flow of meaning;
// in a simple expression evaluator, it'd be the "value",
// but if we're doing something like building a parse tree
// it is the AST itself
type Meaning interface{}

type Parser struct {
}

type Grammar struct {
	productions map[Symbol][]*Production
}

// Productions implements Envt.  This allows a Grammar
// to be used as an Envt in case it's convenient (which
// is what Parse does when the root env is nil)
func (g *Grammar) Productions(nt Symbol) []*Production {
	return g.productions[nt]
}

type Production struct {
	NonTerminal   Symbol
	RightHandSide []AbstractSyntax
	Items         []*Item
	Meaning       Reducer
}

type AbstractSyntax interface {
	String() string
	terminal() bool
}

type TokenMatcher struct {
	eq Token
}

// an Item is a RHS combined with a position within it
// e.g., if a RHS for production is EXPR "+" LITERAL
// then the production gets 4 items, denoted:
//
//     0:   . EXPR   "+"   LITERAL
//     1:     EXPR . "+"   LITERAL
//     2:     EXPR   "+" . LITERAL
//     3:     EXPR   "+"   LITERAL .

type Item struct {
	Production *Production
	Position   int
	Remainder  []AbstractSyntax // rest-of-rhs
}

func (item *Item) next() *Item {
	return item.Production.Items[item.Position+1]
}

func (item *Item) completable() bool {
	return len(item.Remainder) == 0
}

func (item *Item) scannable() bool {
	return item.Remainder[0].terminal()
}

type Reducer interface{}
type ReducerFunc func(Envt, []Meaning) (Envt, Meaning)

// A Formal represents something that can appear on the
// right-hand side of a production.
//
// this could be either
// 1. a Symbol, which denotes a grammar production
// 2. a string, which denotes a literal token (e.g., an operator)
// 3. a Matcher (i.e., an interface which includes a Match method
//    which is a predicate on tokens (i.e., a function Token -> bool))
type Formal interface{}

type Lang struct {
	start   Symbol
	lexer   ScannerFactory
	grammar *Grammar
}

func NewLang(start Symbol, lex ScannerFactory) *Lang {
	return &Lang{
		start: start,
		lexer: lex,
		grammar: &Grammar{
			productions: make(map[Symbol][]*Production),
		},
	}
}

func (l *Lang) Add(p *Production) {
	n := p.NonTerminal
	g := l.grammar
	g.productions[n] = append(g.productions[n], p)
}

func (l *Lang) scan1(text string) (Token, error) {

	if single, ok := l.lexer.(Singler); ok {
		return single.Single(text)
	}

	s := l.lexer.Open(bytes.NewReader([]byte(text)))
	tok, err := s.Next()
	if err != nil {
		return nil, err
	}
	t2, err := s.Next()
	if err == nil {
		if Trace {
			log.Error("%s follows %s", t2, tok)
		}
		return nil, ErrMoreThanOneToken
	}
	if err != io.EOF {
		return nil, err
	}
	return tok, nil
}

var ErrMoreThanOneToken = errors.New("more than one token")

/*type advancer interface {
	advance(Meaning, Token) (Meaning, Envt)
}

*/

type tokCategory struct {
	m Matcher
}

func (tc tokCategory) terminal() bool { return true }
func (tc tokCategory) String() string { return tc.m.String() }

type tokLiteral struct {
	tok Token
}

func (ls tokLiteral) terminal() bool { return true }

func (ls tokLiteral) String() string {
	return fmt.Sprintf("%q", ls.tok.ItemString())
}

func (l *Lang) makeLiteralFormal(s string) AbstractSyntax {
	tok, err := l.scan1(s)
	if err != nil {
		panic(fmt.Sprintf("bad token literal: %s", err))
	}
	return tokLiteral{tok}
}

type nonterminal struct {
	name Symbol
}

func (nt nonterminal) terminal() bool { return false }

func (nt nonterminal) String() string {
	return nt.name.String()
}

// Constructs a new production for use in the receiver language.
// The LHS is the symbol, the formals are the RHS, and the reducer
// should be a function:
//
//	(Envt, []Meaning) -> (Envt, Meaning)
//
// See documentation for Formal for what formals should be
func (l *Lang) Rule(name Symbol, reducer ReducerFunc, formals ...Formal) *Production {
	if reducer == nil {
		panic("must supply reducer")
	}
	rhs := make([]AbstractSyntax, len(formals))

	for i, f := range formals {
		if str, ok := f.(string); ok {
			rhs[i] = l.makeLiteralFormal(str)
		} else if matcher, ok := f.(Matcher); ok {
			rhs[i] = tokCategory{matcher}
		} else if sym, ok := f.(Symbol); ok {
			rhs[i] = nonterminal{sym}
		} else {
			panic(fmt.Sprintf("invalid formal[%d] %#v", i, f))
		}
	}
	items := make([]*Item, len(rhs)+1)
	p := &Production{
		NonTerminal:   name,
		RightHandSide: rhs,
		Items:         items,
		Meaning:       reducer,
	}
	for i := 0; i <= len(rhs); i++ {
		items[i] = &Item{
			Production: p,
			Position:   i,
			Remainder:  rhs[i:],
		}
	}

	return p
}

type tupleKey struct {
	item   *Item
	parent *parseState
	env    Envt
}

func (tk tupleKey) String() string {
	itemv := reflect.ValueOf(tk.item)
	parentv := reflect.ValueOf(tk.parent)
	envv := reflect.ValueOf(&tk.env).Elem()
	id := envv.InterfaceData()
	return fmt.Sprintf("<I%d,S%d,E%d:%d>",
		shorten(itemv.Pointer()),
		shorten(parentv.Pointer()),
		shorten(id[0]), shorten(id[1]))
}

func (tk tupleKey) completable() bool {
	return tk.item.completable()
}

func (tk tupleKey) scannable() bool {
	return tk.item.scannable()
}

type tuple struct {
	key   tupleKey
	accum []Meaning
}

func (t *tuple) String() string {
	buf := bytes.Buffer{}
	buf.WriteByte('{')
	fmt.Fprintf(&buf, "(%s)", t.key.item.Production.NonTerminal)
	rhs := t.key.item.Production.RightHandSide
	for j, as := range rhs {
		if j == t.key.item.Position {
			buf.Write([]byte{' ', '.'})
		}
		buf.WriteByte(' ')
		buf.WriteString(as.String())
	}
	if len(rhs) == t.key.item.Position {
		buf.Write([]byte{' ', '.'})
	}
	buf.WriteByte('}')

	return buf.String()
}

type parseState struct {
	inGrammar *Grammar
	//currentToken Token
	active   []*tuple
	scan     []*tuple
	complete []*tuple
}

func (t *tuple) runCompleter(visit Visitor) {
	// prod is the production we're completing
	prod := t.key.item.Production
	nt := prod.NonTerminal
	if Trace {
		log.Info("reducing %s", t)
	}
	means := applyReducer(prod.Meaning, t.accum)
	if Trace {
		// if there are no active parents, that indicates that the
		// start rule directly expands to what we're matching... we
		// seem to want to have a level of indirection to give meaning
		// to the program.  You can't say:
		//
		//    start -> thing thing thing
		//
		// you need to say:
		//
		//    start -> something
		//    something -> thing thing thing
		//
		// I consider this a bug probably related to my handling
		// of the base case, but the workaround is sufficiently
		// expedient
		log.Info("%d active parents", len(t.key.parent.active))
	}
	for _, check := range t.key.parent.active {
		if check.nextIsForNonTerminal(nt) {
			t := check.advanceTuple(means)
			// advanceTuple() can return nil if the meaning function
			// determines there is no applicable meaning
			if t != nil {
				visit(t)
			}
		}
	}
}

func (t *tuple) runPredictor(from *parseState, visit func(*tuple)) {
	inEnvt := t.key.env
	next := t.key.item.Remainder[0].(nonterminal)
	prods := inEnvt.Productions(next.name)
	if len(prods) == 0 {
		// should we just consider this a "no expansions", e.g.,
		// if there is a grammar rule for which there are no
		// possible expansions?
		panic(fmt.Sprintf("Non-terminal %q not defined", next.name))
	}

	for _, p := range prods {
		// predict by visiting the tuple corresponding to the
		// initial item.  Since it's the initial item, the accum
		// is by definition empty
		key := tupleKey{
			item:   p.Items[0],
			env:    inEnvt,
			parent: from,
		}
		visit(&tuple{key: key})
	}
}

func applyReducer(m Reducer, redux []Meaning) func(Envt, []Meaning) (Envt, Meaning) {
	if f, ok := m.(ReducerFunc); ok {
		if Trace {
			log.Info("using a real reducer function")
		}
		return func(e Envt, _ []Meaning) (Envt, Meaning) {
			return f(e, redux)
		}
	}

	// we are supplying a meaning and environment for where this
	// production appears in a rule; this production is comprised
	// of redux meanings (where len(redux) == # items in this
	// production)
	if m != nil {
		panic(m)
	}
	return func(e Envt, previously []Meaning) (Envt, Meaning) {
		if Trace {
			log.Info("** |reduction|=%d previous=%#v", len(redux), previously)
		}
		return e, 123
	}
}

type MeaningFn func(Envt, []Meaning) (Envt, Meaning)

func (t *tuple) advanceTuple(means MeaningFn) *tuple {
	newEnv, m := means(t.key.env, t.accum)
	if Trace {
		log.Info("means() => %#v", m)
	}
	if m == nil {
		if Trace {
			log.Info("no applicable meaning; not advancing %s", t)
		}
		return nil
	}

	nextItem := t.key.item.next()
	return &tuple{
		key: tupleKey{
			item:   nextItem,
			parent: t.key.parent,
			env:    newEnv,
		},
		accum: append(t.accum, m),
	}
}

// nextIsForNonTerminal checks to see if the thing after the
// dot in the item (which by construction is a nonterminal, because
// we are scanning the active list) is for the given nt symbol
func (t *tuple) nextIsForNonTerminal(nt Symbol) bool {
	// we should only be invoked when the next is a NT,
	// so intentionally panic on conversion if not the case
	next := t.key.item.Remainder[0].(nonterminal)
	return next.name == nt
}

func (t *tuple) scanMeaning(tok Token) (Envt, Meaning, bool) {
	next := t.key.item.Remainder[0]
	switch next := next.(type) {
	case tokLiteral:
		if tok.Equal(next.tok) {
			return nil, tok, true
		}

	case tokCategory:
		if next.m.Match(tok) {
			return nil, tok, true
		}
	default:
		panic("weird scannable")
	}
	return nil, nil, false
}

func (ps *parseState) advanceInput(tok Token) (*parseState, bool) {
	next := []*tuple{}
	for _, t := range ps.scan {
		newEnv, m, ok := t.scanMeaning(tok)
		if !ok {
			if Trace {
				log.Debug("did not scan %s", t)
			}
			continue
		}
		if Trace {
			log.Debug("scanned %s => %#v", t, m)
		}
		fn := func(e Envt, _ []Meaning) (Envt, Meaning) {
			if newEnv == nil {
				return e, m
			} else {
				return newEnv, m
			}
		}
		next = append(next, t.advanceTuple(fn))
	}

	return ps.inGrammar.makeParseState(next), true
}

func (g *Grammar) makeParseState( /*tok Token,*/ init []*tuple) *parseState {
	ps := &parseState{
		inGrammar: g,
		//currentToken: tok,
	}

	seen := map[tupleKey]bool{}

	var visit func(*tuple)

	visit = func(t *tuple) {
		if Trace {
			log.Debug("Visit %s ;%s (||=%d)",
				t, t.key, len(t.accum))
		}
		if seen[t.key] /* && !t.key.completable()*/ {
			return
		}
		// several possibilities...
		seen[t.key] = true
		if Trace {
			for k, _ := range seen {
				log.Debug("   table now has ;%s", k)
			}
		}
		switch {

		case t.key.completable():
			if Trace {
				log.Debug("completable!")
			}
			ps.complete = append(ps.complete, t)
			// the act of
			t.runCompleter(visit)

		case t.key.scannable():
			if Trace {
				log.Debug("scannable!")
			}
			ps.scan = append(ps.scan, t)

		default:
			// an active tuple
			if Trace {
				log.Debug("active!")
			}
			ps.active = append(ps.active, t)
			t.runPredictor(ps, visit)
		}
	}

	for _, t := range init {
		visit(t)
	}

	return ps
}

type grammarInContext struct {
	g   *Grammar
	ctx context.Context
}

func (gic *grammarInContext) Context() context.Context {
	return gic.ctx
}

func (gic *grammarInContext) Productions(nt Symbol) []*Production {
	return gic.g.Productions(nt)
}

// the initial environment can be nil
func (l *Lang) Parse(ctx context.Context, src io.Reader, root Envt) ([]Meaning, error) {
	if root == nil {
		root = &grammarInContext{l.grammar, ctx}
	}

	scan := l.lexer.Open(src)

	ps0 := &parseState{}

	// add the first item of each production associated
	// with the start symbol to the parse state
	boot := []*tuple{}
	for _, p := range l.grammar.productions[l.start] {
		t := &tuple{
			key: tupleKey{
				item:   p.Items[0],
				env:    root,
				parent: ps0,
			},
		}
		boot = append(boot, t)
	}

	// set up the initial parse state
	ps := l.grammar.makeParseState(boot)

	// advance the parse state until we're done
	i := 0
	for {
		if Trace {
			ps.Debug(i)
		}

		tok, err := scan.Next()
		if err != nil {
			if err == io.EOF {
				break
			}
			return nil, err
		}
		if Trace {
			log.Debug("scanning: %s", tok)
		}

		var ok bool
		ps, ok = ps.advanceInput(tok)
		if !ok {
			return nil, ErrParseError
		}
		if ps.dead() {
			return nil, ErrParseErrorAfter{tok}
		}
		i++
	}

	// (??) filter out completions that are not of the start symbol
	var comp []*tuple
	for _, t := range ps.complete {
		if t.key.item.Production.NonTerminal == l.start {
			comp = append(comp, t)
		}
	}

	// return the result
	if len(comp) == 0 {
		return nil, ErrNoCompleteParse
	}
	if len(comp) > 1 {
		if Trace {
			for _, c := range ps.complete {
				log.Warning("Complete: %s", c)
			}
		}
		return nil, ErrAmbiguousParse
	}
	return comp[0].accum, nil
}

func (ps *parseState) dead() bool {
	return len(ps.active) == 0 &&
		len(ps.scan) == 0 &&
		len(ps.complete) == 0
}

func (ps *parseState) Debug(i int) {
	me := shorten(reflect.ValueOf(ps).Pointer())

	log.Debug("=================================[ %d ]=== S%d (dead=%t)",
		i, me, ps.dead())
	log.Debug("%d active:", len(ps.active))
	for j, t := range ps.active {
		log.Debug("   (%d) %s ;%s", j, t, t.key)
	}
	log.Debug("%d scannable:", len(ps.scan))
	for j, t := range ps.scan {
		log.Debug("   (%d) %s ;%s", j, t, t.key)
	}
	log.Debug("%d complete:", len(ps.complete))
	for j, t := range ps.complete {
		log.Debug("   (%d) %s ;%s", j, t, t.key)
	}
}

var ErrNoCompleteParse = errors.New("no complete parse")
var ErrAmbiguousParse = errors.New("multiple complete parses")
var ErrParseError = errors.New("parse error")

type ErrParseErrorAfter struct {
	Token
}

func (err ErrParseErrorAfter) Error() string {
	return fmt.Sprintf("parse error after %s", err.Token)
}

type Visitor func(*tuple)

/*
(define (run-completer (self <tuple>) visit)
  (let* ((prod (production (item self)))
         (nt (non-terminal prod))
         ;; this `means' is a function mapping an advancing tuple's
         ;; environment to a new environment and accumuland (two values)
         (means (apply (action prod) (reverse (accum self)))))
    (let loop ((a (active-tuples (parent-state self))))
      (if (null? a)
          (values)
          (let (((t <tuple>) (car a)))
            (if (eq? (car (rest-of-rhs (item t))) nt)
                (visit (advance-tuple t means)))
            (loop (cdr a)))))))
*/
