// This file provides a built in scanner out of regexes
package earley

import (
	"bytes"
	"errors"
	"io"
	"io/ioutil"
	"math/big"
	"regexp"
	"strconv"
	"unicode/utf8"
)

var ErrEncodingError = errors.New("utf-8 encoding error")
var ErrUnterminatedString = errors.New("unterminated string")
var ErrStringBadEscape = errors.New("bad escape sequence in string")

var ScanTrace = false

type retoken struct {
	restr      string
	stringmode int
	pattern    *regexp.Regexp
	mklex      func(Posn, [][]byte) Token
}

type RegexLang struct {
	dispatch [128][]*retoken
	all      []*retoken
}

func NewRegexLang() *RegexLang {
	return &RegexLang{}
}

func (rel *RegexLang) insert(can string, t *retoken) {
	rel.all = append(rel.all, t)
	re := regexp.MustCompile(can)
	for i := byte(0); i < 128; i++ {
		if re.Match([]byte{i}) {
			rel.dispatch[i] = append(rel.dispatch[i], t)
		}
	}
}

func (rel *RegexLang) AddBuiltinString(can string, flags int) {
	rel.insert(can, &retoken{
		restr:      can + "<STRING>" + can,
		stringmode: flags | 1,
		mklex: func(at Posn, buf [][]byte) Token {
			return &String{
				At:    at,
				Value: string(buf[1]),
			}
		},
	})
}

func (rel *RegexLang) AddSpace(can, pat string) {
	t := &retoken{
		restr:   pat,
		pattern: regexp.MustCompile(pat),
	}
	rel.insert(can, t)
}

func (rel *RegexLang) AddOp(can, pat string, code OpCode) {
	t := &retoken{
		restr:   pat,
		pattern: regexp.MustCompile(pat),
		mklex: func(at Posn, _ [][]byte) Token {
			return &Operator{
				At:   at,
				Code: code,
			}
		},
	}
	rel.insert(can, t)
}

// a factory concept, but based on an instance instead of a class method
// since there are no class methods in Go
type manufacturer interface {
	manufacture(at Posn, lex [][]byte) Token
}

func (rel *RegexLang) AddBuiltin(can, pat string, example Token) {
	cu := example.(manufacturer)
	t := &retoken{
		restr:   pat,
		pattern: regexp.MustCompile(pat),
		mklex: func(at Posn, lexeme [][]byte) Token {
			return cu.manufacture(at, lexeme)
		},
	}
	rel.insert(can, t)
}

type RegexScanner struct {
	src  []byte
	at   Posn
	lang *RegexLang
}

func (t *retoken) scanAsString(at Posn, src []byte) ([][]byte, error) {
	p := src

	start := rune(p[0])
	n := 1

	escaped := false
	escapenummode := 0
	escapenumindex := 0
	escapenumgoal := 0
	var escapenum [8]byte

	accum := &bytes.Buffer{}

	for len(src) > 0 {
		r, size := utf8.DecodeRune(p[n:])
		if r == utf8.RuneError {
			return nil, ErrEncodingError
		}
		n += size
		if escaped {
			if escapenumgoal > 0 {
				if escapenummode == 8 {
					if r >= '0' && r <= '7' {
						escapenum[escapenumindex] = byte(r)
					} else {
						return nil, ErrStringBadEscape
					}
				} else {
					if (r >= '0' && r <= '9') ||
						(r >= 'a' && r <= 'f') ||
						(r >= 'A' && r <= 'F') {
						escapenum[escapenumindex] = byte(r)
					} else {
						return nil, ErrStringBadEscape
					}
				}
				escapenumindex++
				if escapenumindex == escapenumgoal {
					// should not get an error... we only put good stuff in
					n, _ := strconv.ParseUint(
						string(escapenum[:escapenumgoal]),
						escapenummode,
						32)
					accum.WriteRune(rune(n))
					escapenumgoal = 0
					escaped = false
				}
				continue
			}

			escaped = false
			// a few standard special cases
			switch r {
			case 'a':
				accum.WriteByte(0x0007) // alert or bell
			case 'b':
				accum.WriteByte(0x0008) // backspace
			case 'f':
				accum.WriteByte(0x000C) // form feed
			case 'n':
				accum.WriteByte(0x000A) // line feed or newline
			case 'r':
				accum.WriteByte(0x000D) // carriage return
			case 't':
				accum.WriteByte(0x0009) // horizontal tab
			case 'v':
				accum.WriteByte(0x000b) // vertical tab
			case '\\':
				accum.WriteByte(0x005c) // backslash
			case '"':
				accum.WriteByte(0x0022) // double quote
			case '0', '1', '2', '3', '4', '5', '6', '7':
				escapenumindex = 1
				escapenumgoal = 3 // three total
				escapenum[0] = byte(r)
				escapenummode = 8
				escaped = true

			case 'x':
				escapenummode = 16
				escapenumindex = 0
				escapenumgoal = 2
				escaped = true

			case 'u':
				escapenummode = 16
				escapenumindex = 0
				escapenumgoal = 4
				escaped = true

			case 'U':
				escapenummode = 16
				escapenumindex = 0
				escapenumgoal = 8
				escaped = true

			default:
				accum.WriteRune(r)
			}
		} else if r == start {
			return [][]byte{src[:n], accum.Bytes()}, nil
		} else if r == '\\' {
			escaped = true
		} else {
			accum.WriteRune(r)
		}
	}
	return nil, ErrUnterminatedString
}

func (ss *RegexScanner) bestmatch(lst []*retoken) (*retoken, [][]byte, error) {

	var bestt *retoken
	var bestsub [][]byte
	matchlen := 0
	src := ss.src

	for _, t := range lst {
		if ScanTrace {
			log.Info("  checking %q", t.restr)
		}
		var sub [][]byte
		var err error

		if t.stringmode != 0 {
			sub, err = t.scanAsString(ss.at, src)
		} else {
			sub = t.pattern.FindSubmatch(src)
		}

		if err != nil {
			return nil, nil, err
		}
		if sub != nil {
			if len(sub[0]) > matchlen {
				matchlen = len(sub[0])
				bestt = t
				bestsub = sub
			}
		}
	}
	return bestt, bestsub, nil
}

func (ss *RegexScanner) next() (*retoken, [][]byte, error) {
	src := ss.src
	if len(src) == 0 {
		return nil, nil, io.EOF
	}
	if src[0] < 128 {
		fast := ss.lang.dispatch[src[0]]
		if ScanTrace {
			log.Info("%d patterns dispatching on '%c'", len(fast), src[0])
		}

		t, sub, err := ss.bestmatch(fast)
		if err != nil {
			return nil, nil, err
		}
		if sub != nil {
			return t, sub, nil
		}
	}

	t, sub, err := ss.bestmatch(ss.lang.all)
	if err != nil {
		return nil, nil, err
	}
	if sub != nil {
		return t, sub, nil
	}

	if ScanTrace {
		log.Info("No match at %q", src)
	}

	return nil, nil, &BadToken{ss.at}
}

func (ss *RegexScanner) Next() (Token, error) {
	for {
		t, sub, err := ss.next()
		if err != nil {
			return nil, err
		}
		at := ss.at
		// count chars and lines (note we iterate over the string,
		// which goes by runes instead of bytes)
		for _, r := range string(sub[0]) {
			if r == '\n' {
				ss.at.Line++
				ss.at.Column = 0
			} else {
				ss.at.Column++
			}
		}

		ss.src = ss.src[len(sub[0]):]
		if t.mklex != nil {
			tok := t.mklex(at, sub)
			return tok, nil
		}
	}
}

func (rel *RegexLang) Open(src io.Reader) Scanner {
	buf, err := ioutil.ReadAll(src)
	if err != nil {
	}
	return &RegexScanner{
		src:  buf,
		lang: rel,
		at: Posn{
			Line:   1,
			Column: 0,
		},
	}
}

func (id *Identifier) manufacture(at Posn, lex [][]byte) Token {
	return &Identifier{
		At:    at,
		Value: string(lex[0]),
	}
}

func (id *Integer) manufacture(at Posn, lex [][]byte) Token {
	n := &big.Int{}
	n.SetString(string(lex[0]), 10)

	return &Integer{
		At:    at,
		Value: n,
	}
}
