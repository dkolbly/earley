// This file provides a built in scanner for languages with a simple
// Go-like lexical structure
package earley

import (
	"bufio"
	"bytes"
	"fmt"
	"io"
	"math/big"
	"strings"
	"unicode"
	"unicode/utf8"

	"github.com/dkolbly/logging"
)

var log = logging.New("earley")

type Token interface {
	ItemString() string
	Equal(Token) bool
}

type Matcher interface {
	String() string
	Match(Token) bool
}

type BadToken struct {
	At Posn
}

func (bad *BadToken) Error() string {
	return fmt.Sprintf("%s: unexpected token", bad.At)
}

type Posn struct {
	File   string
	Line   int
	Column int
}

func (p Posn) String() string {
	return fmt.Sprintf("%s:%d:%d", p.File, p.Line, p.Column)
}

type Scanner interface {
	Next() (Token, error)
}

// a scanner factory can optionally implement Singler, in which case
// it is used to identify single tokens supplied as part of rules;
// this is useful in cases where the scanner adds tokens on its own,
// e.g., go/scanner will add a ";" token after some tokens, which
// confuses our builtin attempt to use the scanner to scan a single
// token
type Singler interface {
	Single(string) (Token, error)
}

type ScannerFactory interface {
	Open(io.Reader) Scanner
}

//

type SimpleScanner struct {
	src         *bufio.Reader
	at          Posn
	unreadRune  rune
	unreadValid bool
	unreadAt    Posn
	unreadErr   error
}

type SkipComments struct {
	under Scanner
}

func NewSkipComments(from Scanner) Scanner {
	return &SkipComments{
		under: from,
	}
}

func (skip *SkipComments) Next() (Token, error) {
	for {
		tok, err := skip.under.Next()
		if err == nil {
			if _, comment := tok.(*Comment); comment {
				continue
			}
		}
		return tok, err
	}
}

func (ss *SimpleScanner) Next() (Token, error) {
	for {
		tok, err := ss.next()
		// Space returns token==nil

		// TODO should have a skip-comments scanner middleware
		// that we can apply, instead of just hardcoding it here
		if err == nil {
			if _, comment := tok.(*Comment); comment {
				continue
			}
		}
		if tok != nil || err != nil {
			return tok, err
		}
	}
}

// note, this doesn't handle the case that a prefix
// is not a valid operator
func (ss *SimpleScanner) operator(ch rune, at Posn) (Token, error) {
	var accum [256]byte
	buf := accum[:]
	nbytes := 0
	nbytes += utf8.EncodeRune(buf[nbytes:], ch)
	op := string(buf[:nbytes])

	for {
		ch, _, err := ss.readch()
		if err != nil {
			break
		}
		more := utf8.EncodeRune(buf[nbytes:], ch)
		opq := string(buf[:nbytes+more])
		_, ok := operator[opq]
		if !ok {
			break
		}
		// commit to at least this operator
		op = opq
		nbytes += more
	}
	ss.unread()
	if operator[op] == LineComment {
		return ss.linecomment(at)
	} else if operator[op] == BlockComment {
		return ss.blockcomment(at)
	}

	return &Operator{at, operator[op]}, nil
}

func (ss *SimpleScanner) blockcomment(at Posn) (Token, error) {
	var buf bytes.Buffer
	state := 0
	for {
		ch, _, err := ss.readch()
		if err != nil {
			break
		}
		if ch == '*' {
			if state == 1 {
				buf.WriteByte('*')
			}
			state = 1
		} else if ch == '/' && state == 1 {
			break
		} else {
			if state == 1 {
				buf.WriteByte('*')
			}
			state = 0
			buf.WriteRune(ch)
		}
	}
	//log.Debug("Comment is %q", comment)
	return &Comment{at, buf.String()}, nil
}

func (ss *SimpleScanner) linecomment(at Posn) (Token, error) {
	var comment []byte
	for {
		ch, _, err := ss.readch()
		if err != nil {
			break
		}
		if ch == '\n' {
			break
		}
		var runedata [8]byte
		r := runedata[:utf8.EncodeRune(runedata[:], ch)]
		comment = append(comment, r...)
	}
	//log.Debug("Comment is %q", comment)
	return &Comment{at, string(comment)}, nil
}

type OpCode int

const (
	XXSpace = OpCode(iota)
	OpenParen
	CloseParen
	Plus
	Minus
	Times
	Equals
	PlusEquals
	Divide
	DivideEquals
	LineComment
	BlockComment
	Arrow
	Colon
	Dollar
	Dot
	Comma
	Semicolon
	OpenBrace
	CloseBrace
	OpenBracket
	CloseBracket
)

var operator = map[string]OpCode{
	"(":  OpenParen,
	")":  CloseParen,
	"{":  OpenBrace,
	"}":  CloseBrace,
	"[":  OpenBracket,
	"]":  CloseBracket,
	":":  Colon,
	";":  Semicolon,
	"+":  Plus,
	"+=": PlusEquals,
	"=":  Equals,
	"*":  Times,
	".":  Dot,
	"$":  Dollar,
	"/":  Divide,
	"/=": DivideEquals,
	"//": LineComment,
	"/*": BlockComment,
	"->": Arrow,
	",":  Comma,
}

func (ss *SimpleScanner) identifier(ch rune, at Posn) (Token, error) {
	var accum [256]byte
	buf := accum[:]
	nbytes := 0
	nbytes += utf8.EncodeRune(buf[nbytes:], ch)

	for {
		ch, _, err := ss.readch()
		if err != nil || !identifierContinue(ch) {
			break
		}
		nbytes += utf8.EncodeRune(buf[nbytes:], ch)
	}
	ss.unread()
	return &Identifier{at, string(buf[:nbytes])}, nil
}

func (ss *SimpleScanner) number(ch rune, at Posn) (Token, error) {
	accum := big.NewInt(int64(ch) - '0')
	for {
		ch, _, err := ss.readch()
		if !unicode.IsDigit(ch) || err != nil {
			break
		}
		accum.Mul(accum, big.NewInt(10))
		accum.Add(accum, big.NewInt(int64(ch)-'0'))
	}
	ss.unread()
	return &Integer{at, accum}, nil
}

func (ss *SimpleScanner) quoted(ch rune, at Posn) (Token, error) {
	accum := &bytes.Buffer{}
	for {
		ch, _, err := ss.readch()
		if err != nil {
			return nil, err
		}
		if ch == '"' {
			break
		}
		accum.WriteRune(ch)
	}
	return &String{At: at, Value: accum.String()}, nil
}

type Comment struct {
	At      Posn
	Comment string
}

func (c *Comment) ItemString() string {
	return `//` + c.Comment
}

func (c *Comment) Equal(t Token) bool {
	if c2, ok := t.(*Comment); ok {
		return strings.TrimSpace(c.Comment) == strings.TrimSpace(c2.Comment)
	}
	return false
}

func (c *Comment) String() string {
	return fmt.Sprintf("@%d:%d COMMENT{%s}",
		c.At.Line,
		c.At.Column,
		c.Comment)
}

type Operator struct {
	At   Posn
	Code OpCode
}

func (op *Operator) ItemString() string {
	for k, v := range operator {
		if v == op.Code {
			return k
		}
	}
	return "?"
}

func (op *Operator) Equal(t Token) bool {
	if op2, ok := t.(*Operator); ok {
		return op.Code == op2.Code
	}
	return false
}

func (op *Operator) String() string {

	for k, v := range operator {
		if v == op.Code {
			return fmt.Sprintf("@%d:%d OP{%s}",
				op.At.Line,
				op.At.Column,
				k)
		}
	}
	return fmt.Sprintf("@%d:%d OP?",
		op.At.Line,
		op.At.Column)
}

type Identifier struct {
	At    Posn
	Value string
}

func (id *Identifier) ItemString() string {
	return id.Value
}

func (id *Identifier) Equal(t Token) bool {
	if i2, ok := t.(*Identifier); ok {
		return id.Value == i2.Value
	}
	return false
}

func (id *Identifier) String() string {
	return fmt.Sprintf("@%d:%d ID{%s}", id.At.Line, id.At.Column,
		id.Value)
}

type String struct {
	At    Posn
	Value string
}

func (str *String) ItemString() string {
	return fmt.Sprintf("%q", str.Value)
}

func (str *String) Equal(t Token) bool {
	if s2, ok := t.(*String); ok {
		return str.Value == s2.Value
	}
	return false
}

func (str *String) String() string {
	return fmt.Sprintf("@%d:%d STR{%q}",
		str.At.Line, str.At.Column,
		str.Value)
}

func (ss *SimpleScanner) unread() {
	ss.unreadValid = true
}

func (ss *SimpleScanner) readch() (rune, Posn, error) {
	if ss.unreadValid {
		//log.Debug("Re-read at %s", ss.unreadAt)
		ss.unreadValid = false
		return ss.unreadRune, ss.unreadAt, ss.unreadErr
	}

	at := ss.at

	//log.Debug("Reading at %s", at)
	ch, _, err := ss.src.ReadRune()
	if err != nil {
		ss.unreadErr = err
		return 0, at, err
	}
	//log.Debug("Read %q", ch)
	if ch == '\n' {
		ss.at.Line++
		ss.at.Column = 0
	} else {
		ss.at.Column++
	}
	ss.unreadRune = ch
	ss.unreadValid = false
	ss.unreadAt = at
	return ch, at, nil
}

func (ss *SimpleScanner) next() (Token, error) {
	ch, at, err := ss.readch()
	if err != nil {
		return nil, err
	}

	var cat Category

	if ch <= unicode.MaxLatin1 {
		cat = latinCategory[ch]
	} else {
		cat = categorize(ch)
	}

	switch cat {
	case Space:
		return nil, nil
	case Number:
		return ss.number(ch, at)
	case Ident:
		return ss.identifier(ch, at)
	case Op:
		return ss.operator(ch, at)
	case Quoted:
		return ss.quoted(ch, at)
	case Invalid:
		// fall through
	}
	return nil, &BadToken{at}
}

func categorize(ch rune) Category {
	if unicode.IsSpace(ch) {
		return Space
	}
	if ch >= '0' && ch <= '9' {
		return Number
	}

	if identifierInitial(ch) {
		return Ident
	}
	switch ch {
	// TODO, '-' should be special... could be a negative number
	// also '.' for floats
	case '(', ')', '+', '-', '&', '=', '*', '/', ':', ';', '{', '}', '[', ']', '$', '.':
		return Op
	case '"':
		return Quoted
	}

	return Invalid
}

func identifierInitial(ch rune) bool {
	return unicode.IsLetter(ch) || ch == '_'
}

func identifierContinue(ch rune) bool {
	return identifierInitial(ch) || unicode.IsDigit(ch)
}

type Category uint8

const (
	Space = Category(iota)
	Invalid
	Number
	Op
	Ident
	Quoted
)

var latinCategory [256]Category

func init() {
	for ch := rune(0); ch < 256; ch++ {
		latinCategory[ch] = categorize(ch)
	}
}

func NewSimple(name string, src io.Reader) *SimpleScanner {
	return &SimpleScanner{
		src: bufio.NewReader(src),
		at: Posn{
			File:   name,
			Line:   1,
			Column: 0,
		},
	}
}

type SimpleFactory struct {
}

func (sf SimpleFactory) Open(src io.Reader) Scanner {
	return NewSimple("", src)
}
