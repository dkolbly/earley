package earley

import (
	"bytes"
	"io"
	"math/big"
	"reflect"
	"testing"
)

type scanTestCase struct {
	file   string
	input  string
	output []Token
}

var scanTests = []scanTestCase{
	{
		file:  "t0",
		input: "foë bar\n baz",
		output: []Token{
			&Identifier{
				At: Posn{
					File:   "t0",
					Line:   1,
					Column: 0,
				},
				Value: "foë",
			},
			&Identifier{
				At: Posn{
					File:   "t0",
					Line:   1,
					Column: 4,
				},
				Value: "bar",
			},
			&Identifier{
				At: Posn{
					File:   "t0",
					Line:   2,
					Column: 1,
				},
				Value: "baz",
			},
		},
	},
	{
		file:  "t1",
		input: `37:"foë"`,
		output: []Token{
			&Integer{
				At: Posn{
					File:   "t1",
					Line:   1,
					Column: 0,
				},
				Value: big.NewInt(37),
			},
			&Operator{
				At: Posn{
					File:   "t1",
					Line:   1,
					Column: 2,
				},
				Code: Colon,
			},
			&String{
				At: Posn{
					File:   "t1",
					Line:   1,
					Column: 3,
				},
				Value: "foë",
			},
		},
	},
}

func TestSuccessfulScans(t *testing.T) {
	for i, st := range scanTests {
		log.Debug("Scanner test case[%d]", i)
		scanner := NewSimple(st.file, bytes.NewReader([]byte(st.input)))
		var results []Token
		for {
			tok, err := scanner.Next()
			if err != nil {
				if err == io.EOF {
					break
				}
				t.Fatalf("Expected successful scan, got error: %s", err)
			}
			results = append(results, tok)
		}
		log.Debug("scanned %d tokens: %#v", len(results), results)
		if len(results) != len(st.output) {
			t.Fatalf("Expected %d tokens, got %d results", len(st.output), len(results))
		}
		for i, exp := range st.output {
			if !reflect.DeepEqual(exp, results[i]) {
				t.Fatalf("Expected token[%d] to be %#v, got %#v", i, exp, results[i])
			}
		}
	}

}
