package earley

import (
	"sync"
)

type Symbol struct {
	id int
}

var symbolNames = make(map[Symbol]string, 1000)
var symbolTable = make(map[string]Symbol, 1000)
var symbolTableLock sync.Mutex

func Intern(s string) Symbol {
	symbolTableLock.Lock()
	defer symbolTableLock.Unlock()
	if sym, ok := symbolTable[s]; ok {
		return sym
	}
	sym := Symbol{1 + len(symbolTable)}
	symbolNames[sym] = s
	symbolTable[s] = sym
	return sym
}

func (s Symbol) String() string {
	return symbolNames[s]
}
